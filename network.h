typedef struct queue_ring {
  struct dma_ring_slot* ring; 
  unsigned int head; 
  unsigned int tail; 
  unsigned int print_l; 
} queue_ring; 

//initialized a ring with capacity size
struct dma_ring_slot* ring_init(int size); 

// Initializes the network driver, allocating the space for the ring buffer.
void network_init();

// Starts receiving packets!
void network_start_receive();

void network_print_stats(); 

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt);

// Continually polls for data on the ring buffer. Loops forever!
void network_poll();

// Called when a network interrupt occurs.
void network_trap();

// Shared packet handling between polling and interrupt functionality 
void handle_packet(); 

// Processes command packets
void process_command(struct honeypot_command_packet *p);

// prints the current value of all statistics 
void print_statistics(); 

// Change the endianness of an integer
unsigned long change_endian(unsigned long num); 

