
#include "kernel.h"

#define RING_SIZE 16
#define BUFFER_SIZE 4096 

volatile struct dev_net *dev_net;
struct dma_ring_slot* ring; 
struct hashtable* evil;
struct hashtable* spammers;
struct hashtable* vulnerable;

int byte_total = 0;  //total bytes processed

// Initializes the network driver, allocating the space for the ring buffer.
struct dma_ring_slot* ring_init(int size){

      struct dma_ring_slot* ring = (struct dma_ring_slot*) malloc(sizeof(struct dma_ring_slot)*size); 

      for (int i = 0; i < size; i++){
        void* space = malloc(BUFFER_SIZE); 
        ring[i].dma_base = virtual_to_physical(space);  
        ring[i].dma_len = BUFFER_SIZE; 
      }

      return ring; 
}

void network_init(){
for (int i = 0; i < 16; i++) {
    if (bootparams->devtable[i].type == DEV_TYPE_NETWORK) {
      printf("Detected network device...\n");
      // find a virtual address that maps to this I/O region
      dev_net = physical_to_virtual(bootparams->devtable[i].start); 
      // also allow keyboard interrupts
      
      struct dma_ring_slot* ring = ring_init(RING_SIZE); 
      dev_net->rx_base = virtual_to_physical(ring); 
      dev_net->rx_capacity = RING_SIZE; 
      dev_net->rx_head = 0;
      dev_net->rx_tail = 0;


        // key: int, spammer address 
        // value: int, number of packets received from address
        spammers = (struct hashtable*)malloc(sizeof(struct hashtable)); 
        hashtable_create(spammers, 4);
        //key : int, port number (cast to int)
        //value: int, number of packets received at port
        vulnerable = (struct hashtable*)malloc(sizeof(struct hashtable)); 
        hashtable_create(vulnerable, 4);
        
        //key: int, evil hash 
        //value: int, number of evil packets received
        evil = (struct hashtable*)malloc(sizeof(struct hashtable));
        hashtable_create(evil, 4);
         
        printf("just ran hashtable\n"); 


      printf("...network driver is ready.\n"); 
      return;
    }
  }

 



}

// Starts receiving packets!
void network_start_receive(){
  //turn network card on 
  dev_net->cmd = NET_SET_POWER; 
  dev_net -> data = 1; 
  dev_net->cmd = NET_SET_RECEIVE; 
  dev_net-> data = 1; 
  //start receiving packets 
} 

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt){
  //enable interrupts for the current core 
  dev_net -> cmd = NET_SET_INTERRUPTS; 
  dev_net -> data = opt; 
  set_cpu_status(current_cpu_status() | (1 << (8 + INTR_NETWORK))); //??? what does this line do? 
}


// Continually polls for data on the ring buffer. Loops forever!
void network_poll(){
  int n = 0; 
  while(1){
    if (dev_net -> rx_head != dev_net -> rx_tail){
      n = n % (NUM_CORES-2) ; 
      struct queue_ring* q = queues[n]; 
      if((q->head)%16 == q->tail && q->head != q->tail){
        printf("buffer is full for %d\n", n+1);
      }
      else{
      //get data from ring slot and caste to Honeypot command packet
      //better performance with this here or in each core?
      
      struct honeypot_command_packet* p;
      struct dma_ring_slot* s = physical_to_virtual(dev_net->rx_base); 
      struct dma_ring_slot* offset = s + ((dev_net->rx_tail)%(dev_net->rx_capacity)); 
      p = physical_to_virtual(offset->dma_base); 
      
      
      q->ring[q->head % 16].dma_base = offset->dma_base; 
      q->ring[q->head % 16].dma_len = offset->dma_len; 
      

      //printf("added %x to Core %d at %d\n", p->data_big_endian, n+1, q->head); 
      //printf("%d has buffer length %d\n", current_cpu_id(), offset->dma_len); 
      //printf("Setting %d to buffer length %d\n", n, q->ring[q->head].dma_len); 
      q->head++; 

      n++; 
      byte_total += offset->dma_len; 
      offset->dma_len = BUFFER_SIZE;
      dev_net-> rx_tail++; 
    }
    }
  }
}

// Called when a network interrupt occurs.
void network_trap(){

}

void handle_packet(){
  //access the data at queue[core_id][rx_tail]
  struct honeypot_command_packet* p; 
  struct queue_ring* q = queues[current_cpu_id()-2]; 

  p = physical_to_virtual(q->ring[q->tail % 16].dma_base); 

  unsigned int len = q->ring[q->tail % 16].dma_len; 

  //printf("Core %d: Extracted %x from %d\n", current_cpu_id(), p->data_big_endian, q->tail); 

  //printf("%d has buffer length %d\n", current_cpu_id(), len); 

  if(p->secret_big_endian == 0x1034){
    process_command(p); 
  }
  else {
    unsigned long hash = djb2((unsigned char*)p, len); 
    hash = change_endian(hash); 
    int n = p->headers.udp_dest_port_big_endian << 16;
    mutex_lock(&(spammers->lock)); 
    hashtable_add(spammers, p->headers.ip_source_address_big_endian, p->headers.ip_source_address_big_endian); 
    mutex_unlock(&(spammers->lock));
    mutex_lock(&(vulnerable->lock)); 
    hashtable_add(vulnerable, n, n); 
    mutex_unlock(&(vulnerable->lock));
    mutex_lock(&(evil->lock));
    hashtable_add(evil, hash, hash); 
    mutex_unlock(&(evil->lock)); 
  }
  //printf("%d has buffer length %d\n", current_cpu_id(), q->ring[q->tail].dma_len); 
  q->ring[q->tail].dma_len = BUFFER_SIZE; 
  q->tail++; 
}


void process_command(struct honeypot_command_packet *p){
  switch (p->cmd_big_endian) {
    case HONEYPOT_ADD_SPAMMER:  
       mutex_lock(&(spammers->lock)); 
       hashtable_put(spammers, p->data_big_endian, p->data_big_endian, 0); 
       mutex_unlock(&(spammers->lock)); 
      break; 
    case 0x201: //HONEYPOT_ADD_EVIL: 
      mutex_lock(&(evil->lock)); 
      hashtable_put(evil, p->data_big_endian, p->data_big_endian, 0); 
      mutex_unlock(&(evil->lock)); 
      break;
    case 0x301: //HONEYPOT_ADD_VULNERABLE:
      mutex_lock(&(vulnerable->lock)); 
      hashtable_put(vulnerable, p->data_big_endian, p->data_big_endian, 0);  
      mutex_unlock(&(vulnerable->lock)); 
      break;   
    case 0x102: //HONEYPOT_DEL_SPAMMER:
      mutex_lock(&(spammers->lock)); 
      hashtable_remove(spammers, p->data_big_endian, p->data_big_endian); 
      mutex_unlock(&(spammers->lock)); 
      break; 
    case HONEYPOT_DEL_EVIL:
      mutex_lock(&(evil->lock)); 
      hashtable_remove(evil, p->data_big_endian, p->data_big_endian); 
      mutex_unlock(&(evil->lock)); 
      break; 
    case 0x302: //HONEYPOT_DEL_VULNERABLE: 
      mutex_lock(&(vulnerable->lock)); 
      hashtable_remove(vulnerable, p->data_big_endian, p->data_big_endian); 
      mutex_unlock(&(vulnerable->lock)); 
      break; 
    case 0x103: //HONEYPOT_PRINT:
      print_statistics(); 
      break;  
  }
}

void print_statistics(){
    printf("\n \n"); 
    //spammers
    printf("SPAMMERS: \n"); 
    mutex_lock(&(spammers->lock)); 
    hashtable_stats(spammers); 
    mutex_unlock(&(spammers->lock)); 
    //vulnerable ports
    printf("\nVULNERABLE PORTS: \n"); 
    mutex_lock(&(vulnerable->lock)); 
    hashtable_stats(vulnerable); 
    mutex_unlock(&(vulnerable->lock)); //evil packets 

    printf("\nEVIL PACKETS: \n"); 
    mutex_lock(&(evil->lock)); 
    hashtable_stats(evil); 
    mutex_unlock(&(evil->lock)); 

    // should use floats here but they print garbage values for some reason
    int num_seconds = current_cpu_cycles()/CPU_CYCLES_PER_SECOND;
    int packet_rate = (dev_net->rx_tail) / num_seconds;
    int byte_rate = ((byte_total) / num_seconds);

    printf("\nTotal packets processed: %d\n", dev_net->rx_tail);
    printf("Total bytes processed:   %d\n", byte_total);
    if (num_seconds) {
      printf("Rate (packets/s):        %d\n", packet_rate );
      printf("Rate (Mb/s):             %d.%d\n", byte_rate/100000, byte_rate%100000 );
    }
    printf("CPU cycles:              %d\n", current_cpu_cycles());  
    printf("\n \n"); 
}


unsigned long change_endian(unsigned long num){
    return ((num>>24)&0xff) | // move byte 3 to byte 0
                    ((num<<8)&0xff0000) | // move byte 1 to byte 2
                    ((num>>8)&0xff00) | // move byte 2 to byte 1
                    ((num<<24)&0xff000000); // byte 0 to byte 3

}




