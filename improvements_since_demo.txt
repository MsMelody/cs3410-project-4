Our improvements since the live demo can be lumped into two categories:

  (1) parallelizing code to a 32 core architecture; and
  (2) optimizing the hashtable to avoid being locked in synchronization.

We detail the specific changes we made below.

#######################
# (1) PARALLELIZATION #
#######################

(1.1) Work distribution among cores

Our implementation supports 32 cores (though we must allocate all 12 MB of RAM allowed to us
to do so). Core 0 polls packets from the the ring and passes them to queues (a data structure
very similar to the ring, with one queue associated with every non-polling core). The 31 other
cores process data from their queues and store relevant statistics to the associated
synchronized hashtable.


(1.2) Data structures

The two data structures of note here are QUEUEs and HASHTABLEs.

(1.2.1) Queues

	typedef struct queue_ring {
  		struct dma_ring_slot* ring; 
  		unsigned int head; 
  		unsigned int tail; 
	} queue_ring; 

A queue ring mirrors the structure of the dev_net ring. Core 0 store data at the head, and
the core associated with a queue ring pulls from the tail. This means that queues do not
need synchronization since head > tail whenever a core is handling a packet.	
	
We allocate a queue_ring for each core (besides core 0):
	
	struct queue_ring* queues[NUM_CORES-1]; 
	for (int i = 0; i < NUM_CORES-1; i++){
        queues[i] = (struct queue_ring*)malloc(sizeof(struct queue_ring)); 
        queues[i]->ring = ring_init(16); 
        queues[i]->head = 0; 
        queues[i]->tail = 0; 
    }
	
(1.3) Synchronization
 
* We implemented mutex_lock() and mutex_unlock() functions as follows:
	
	void mutex_lock(int *m){
   __asm__(
     ".set mips2\n\t"
     "TS:\n\t"
     "LI $8, 1\n\t"
     "LL $9, 0($4)\n\t"
     "BNEZ $9, TS\n\t"
     "SC $8, 0($4)\n\t"
     "BEQZ $8, TS\n\t"
     );
    }

    void mutex_unlock(int *m){
   __asm__(
      ".set mips2\n\t"
      "SW $0, 0($4)"
      ); 
    }
   
   These use these locks whenever a hashtable is accessed or printf() is called, and when
   hashtable_rehash() calls malloc().
     ** (We implemented a GNU-licenced printf() variant that supports locking.) 
 * The dev_net ring is only accessed by core 0.
 * Only core 0 uses malloc(), except in cases where a core causes a hashtable to rehash.
 * Queue rings demand head > tail for access, prevent core 0 and the associated core from
   from ever conflicting.
	
(1.4.) UI improvements
We cleaned up the UI to make statistics output look cleaner, and to actually respond to key
presses.

##############################
# (2) HASHTABLE OPTIMIZATION #
##############################

Our original hashtable implemented linear probing with a simple recusrive algorithm as represented
in pseudocode:

	subroutine put(hash, key, value)
		var k = hash % buffer_size
	
		if (bucket[k].is_filled)
			// put new value
			if (load > load_factor)
				rehash()
			endif
		else
			hash(hash+1, key, value)
		endif
	return

get() uses a similar probing algorithm. Since the hashtable is synchronized across 32 cores,
we wanted to spend as little time as possible in put() and get(). A recursive algorithm is
suboptimal for this task (at least in MIPS) because of the constant stack and frame pointer and
memory manipulations involved with calling subroutines.

So we reverted the recursion into a while loop as follows:

	subroutine put(hash, key, value)
		var k = hash % buffer_size
		var i = 0
		var still_looping = true

		while (still_looping AND (i < buffer_size))
			if (bucket[k].is_filled)
				// put new value

				if (load > load_factor)
					rehash()
				endif

				looping = false
			else
				k = k+1 % buffer_size
				i = i+1
			endif
		endwhile
	return

And used a similar while-loop algorithm for get(). We then unrolled this loop slightly in the
same way the djb2() function was slightly optimized by our instructors, hard coding the while
loop to run eight times, and then while looping the remainder number of times.

Doing this reduced the number of resultant MIPS operations required to use the hashtable when
linear probing is required, resulting in the program spending less time locking the hashtable.
