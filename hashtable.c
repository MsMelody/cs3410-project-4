#include "kernel.h"



void hashtable_create(struct hashtable *self, int size){
    self -> buffer = (entry*)malloc(size * sizeof(entry)); 
    int i; 
    for (i = 0; i < size; i++){
      self-> buffer[i].filled = 0; 
      self-> buffer[i].value = 0; 
      self-> buffer[i].key = 0; 
    }
    self -> buffer_size = size; 
    self -> size = 0; 
    self -> lock = 0; 
}

//if the entry at k is already filled, use probing 
//if the entry at k is empty, fill it with value 
// mildly optimized -- traded recursion for slightly unrolled while loop
void hashtable_put(struct hashtable *self, int hash, int key, int value){
    int k = hash % self->buffer_size; 

  /* Loops until:
   *  - empty buffer index; OR
   *  - has linearly probed across entire buffer;
   * whichever comes first. */
  int looping = 1;
  int i = 0;
  while (looping && (i < self->buffer_size - 8)) {
    // empty buffer index
    if (!self->buffer[k].filled) {
      self->buffer[k].value = value;
      self->buffer[k].filled = 1;
      self->buffer[k].key = key;
      self->size++;

      if (2*self->size >= self->buffer_size) hashtable_rehash(self);

      looping = 0;
    }
    else {
        k++;
        k = k % self->buffer_size;
        i++;

        if (!self->buffer[k].filled) {
          self->buffer[k].value = value;
          self->buffer[k].filled = 1;
          self->buffer[k].key = key;
          self->size++;

          if (2*self->size >= self->buffer_size) hashtable_rehash(self);

          looping = 0;
        }
        else {
            // 2 layers into loop
            k++;
            k = k % self->buffer_size;
            i++;

            if (!self->buffer[k].filled) {
              self->buffer[k].value = value;
              self->buffer[k].filled = 1;
              self->buffer[k].key = key;
              self->size++;

              if (2*self->size >= self->buffer_size) hashtable_rehash(self);

              looping = 0;
            }
            else {
                // 3 layers into loop
                k++;
                k = k % self->buffer_size;
                i++;

                if (!self->buffer[k].filled) {
                  self->buffer[k].value = value;
                  self->buffer[k].filled = 1;
                  self->buffer[k].key = key;
                  self->size++;

                  if (2*self->size >= self->buffer_size) hashtable_rehash(self);
                  
                  looping = 0;
                }
                else {
                    // 4 layers into loop
                    k++;
                    k = k % self->buffer_size;
                    i++;

                    if (!self->buffer[k].filled) {
                      self->buffer[k].value = value;
                      self->buffer[k].filled = 1;
                      self->buffer[k].key = key;
                      self->size++;

                      if (2*self->size >= self->buffer_size) hashtable_rehash(self);

                      looping = 0;
                    }
                    else {
                        // 5 layers into loop
                        k++;
                        k = k % self->buffer_size;
                        i++;

                        if (!self->buffer[k].filled) {
                          self->buffer[k].value = value;
                          self->buffer[k].filled = 1;
                          self->buffer[k].key = key;
                          self->size++;

                          if (2*self->size >= self->buffer_size) hashtable_rehash(self);

                          looping = 0;
                        }
                        else {
                            // 6 layers into loop
                            k++;
                            k = k % self->buffer_size;
                            i++;

                            if (!self->buffer[k].filled) {
                              self->buffer[k].value = value;
                              self->buffer[k].filled = 1;
                              self->buffer[k].key = key;
                              self->size++;

                              if (2*self->size >= self->buffer_size) hashtable_rehash(self);

                              looping = 0;
                            }
                            else {
                                // 7 layers into loop
                                k++;
                                k = k % self->buffer_size;
                                i++;

                                if (!self->buffer[k].filled) {
                                  self->buffer[k].value = value;
                                  self->buffer[k].filled = 1;
                                  self->buffer[k].key = key;
                                  self->size++;

                                  if (2*self->size >= self->buffer_size) hashtable_rehash(self);

                                  looping = 0;
                                }
                                else {
                                  // back to top
                                  k++;
                                  k = k % self->buffer_size;
                                  i++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  }

  while (looping && (i < self->buffer_size)) {
    if (!self->buffer[k].filled) {
      self->buffer[k].value = value;
      self->buffer[k].filled = 1;
      self->buffer[k].key = key;
      self->size++;

      if (2*self->size >= self->buffer_size) hashtable_rehash(self);

      looping = 0;
    }
    else {
      k++;
      k = k % self->buffer_size;
      i++;
    }
  }
}

// if the entry at the key is already filled, add one to its count 
// otherwise, do nothing
// mildly optimized -- traded recursion for (unrollable?) while loop
void hashtable_add(struct hashtable *self, int hash, int key){
  int k = hash % self->buffer_size; 
  
  /* Loops until:
   *  - correct buffer index; OR
   *  - empty buffer index; OR
   *  - has looped across entire buffer;
   * whichever comes first. */
  int looping = 1;
  int i = 0;
  while (looping && (i < self->buffer_size)) {
    if (self->buffer[k].filled == 1)  {

      // correct buffer index -- stop
      if (self->buffer[k].key == key) {
        self->buffer[k].value += 1; 
        looping = 0;
      }
      else {
        k++;
        k = k % self->buffer_size;
        i++;
      }

    }
    // empty buffer index -- stop
    else looping = 0;
  }

}

// get must take into account probing. Will start at k and move to the 
// right until either finding the key or finding an unfilled entry
// mildly optimized -- traded recursion for slightly unrolled while loop
int hashtable_get(struct hashtable *self, int hash, int key){
    int k = hash % self -> buffer_size;

    int i = 0;
    while (i < self->buffer_size - 8) {
      // 0 levels extra probing
      if (!self->buffer[k].filled) return -1; 
      else {
        if (self->buffer[k].key == key) return self->buffer[k].value;
        else {
          // 1 level extra probing
          k++;
          k = k % self->buffer_size;
          i++;
          
          if (!self->buffer[k].filled) return -1;
          else {
            if (self->buffer[k].key == key) return self->buffer[k].value;
            else {
              // 2 levels extra probing
              k = k % self->buffer_size;
              k++;
              i++;

              if (!self->buffer[k].filled) return -1;
              else {
                if (self->buffer[k].key == key) return self->buffer[k].value;
                else {
                  // 3 levels extra probing
                  k = k % self->buffer_size;
                  k++;
                  i++;

                  if (!self->buffer[k].filled) return -1;
                  else {
                    if (self->buffer[k].key == key) return self->buffer[k].value;
                    else {
                      // 4 levels extra probing
                      k = k % self->buffer_size;
                      k++;
                      i++;

                      if (!self->buffer[k].filled) return -1;
                      else {
                        if (self->buffer[k].key == key) return self->buffer[k].value;
                        else {
                          // 5 levels extra probing
                          k = k % self->buffer_size;
                          k++;
                          i++;

                          if (!self->buffer[k].filled) return -1;
                          else {
                            if (self->buffer[k].key == key) return self->buffer[k].value;
                            else {
                              // 6 levels extra probing
                              k = k % self->buffer_size;
                              k++;
                              i++;

                              if (!self->buffer[k].filled) return -1;
                              else {
                                if (self->buffer[k].key == key) return self->buffer[k].value;
                                else {
                                  // 7 levels extra probing
                                  k = k % self->buffer_size;
                                  k++;
                                  i++;

                                  if (!self->buffer[k].filled) return -1;
                                  else {
                                    if (self->buffer[k].key == key) return self->buffer[k].value;
                                    else {
                                      // jump back to beginning of loop
                                      k = k % self->buffer_size;
                                      k++;
                                      i++;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    while (i < self->buffer_size) {
      if (!self->buffer[k].filled) return -1;
      else {
        if (self->buffer[k].key == key) return self->buffer[k].value; // again, no need for looping = 0
        else {
          k++;
          k = k % self->buffer_size;
          i++;
        }
      }
    }

    // entire buffer has been probed
    return -1;
} 

//Load factor is .5
//Rehash to double the size  
void hashtable_rehash(struct hashtable *self){
  int keys[self->size]; 
  int values[self->size]; 
  int s = self -> size; 
  int index = 0; 
  int i = 0; 
  for (i = 0; i < self->buffer_size; i++){
    if (self->buffer[i].filled == 1){
      keys[index] = self->buffer[i].key; 
      values[index] = self->buffer[i].value; 
      index++; 
    }
  }

  free(self->buffer); 
  hashtable_create(self, self->buffer_size*2); 

  for (i=0; i < s; i++){
    hashtable_put(self, keys[i], keys[i], values[i]); 
  } 
}

//if the given key is in the hashtable, remove it 
//Otherwise do nothing
void hashtable_remove(struct hashtable *self, int hash, int key){
  int k = hash % self -> buffer_size; 
    if (self->buffer[k].filled == 1){
      if (self->buffer[k].key == key){
        self->buffer[k].filled = 0;
        self->size = self->size - 1;  
      }
      else{
        hashtable_remove(self, k+1, key); 
      }
    }
}


//prints out keys and values in hashtable, 3 per row 
void hashtable_stats(struct hashtable *self){
  int i; 
  int f = 0; 
  for (i = 0; i < self->buffer_size; i++){
    if (self->buffer[i].filled == 1){
      printf("0x%-8x : %4d      ", (int)change_endian(self->buffer[i].key), self->buffer[i].value); 
      f++; 
      if (f == 3){
        f = 0; 
        printf("\n"); 
      }
    }
  }
  printf("\n"); 
}


struct hashtable* hashtable_run(){
  struct hashtable* a = (struct hashtable*)malloc(sizeof(hashtable)); 
  return a; 
}

//hash function, used only for evil packets
unsigned long djb2(unsigned char *pkt, int n)
{
  unsigned long hash = 5381;
  int i = 0;
  while (i < n-8) {
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
    hash = hash * 33 + pkt[i++];
  }
  while (i < n)
    hash = hash * 33 + pkt[i++];
  return hash;
}

