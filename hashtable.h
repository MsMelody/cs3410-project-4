// key[address] -> number 

// num packets processed - rx_tail 
// num packets received - 
// num packets dropped 

// num bytes arriving 

// spammer address -> num packets received

// vulnerable ports -> num packets received 

// evil packet fingerprint -> num packets received 
#include "kernel.h"

typedef struct entry {
  int key; 
  int value; 
  short filled; 
} entry; 

typedef struct hashtable {
  unsigned int size;        //the number of elements I have 
  unsigned int buffer_size; //the number of buckets I have 
  entry* buffer;            //an array of ints
  int lock; 
} hashtable; 

void hashtable_create(struct hashtable *self, int size); 


//if the entry at k is already filled, use probing 
//if the entry at k is empty, fill it with value 
void hashtable_put(struct hashtable *self, int hash, int key, int value); 

// if the entry at the key is already filled, add one to its count 
// otherwise, do nothing 
void hashtable_add(struct hashtable *self,int hash, int key); 
// get must take into account probing. Will start at k and move to the 
// right until either finding the key or finding an unfilled entry 
int hashtable_get(struct hashtable *self, int hash, int key);
void hashtable_rehash(struct hashtable *self); 
void hashtable_remove(struct hashtable *self, int hash, int key); 
void hashtable_stats(struct hashtable *self); 
struct hashtable* hashtable_run(); 

unsigned long djb2(unsigned char *pkt, int n); 
